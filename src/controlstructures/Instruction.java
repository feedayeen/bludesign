package controlstructures;

import datatypes.*;

public class Instruction
{
	Type variable;
	String function;
	Type[] parameters;
	
	Type run()
	{
		return variable.call(function, parameters);
	}
}
