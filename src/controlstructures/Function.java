package controlstructures;

public class Function
{
	private String name;
	private String returnType;
	private String[] parametersType;
	
	public String getName()
	{
		return name;
	}
	
	public String getReturn()
	{
		return returnType;
	}
	
	public String[] parametersType()
	{
		return parametersType;
	}
}
