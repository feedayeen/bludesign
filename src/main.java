import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import datatypes.*;
import datatypes.builtin.*;

public class main
{
	static Type[] array = new Type[10];
	
	public static void test(int... vals)
	{
		System.out.println(vals.length);
	}
	
	public static Type index(int ind)
	{
		return array[ind];
	}
	
	public static void main(String[] args)
	{
		System.out.println("Hello, BluDesign");
		Type tru = TypeFactory.builtin(true);
		Type fal = TypeFactory.builtin(false);
		Type tex = TypeFactory.builtin("text");
		System.out.println(tex);
		System.out.println(tex.call("=", fal).call("+", tru));
		System.out.println(tex.getFunctionNames());
		Type temp = index(1);
		temp = new Bool(true);
		System.out.println(temp);
		System.out.println(index(1));
		TreeSet<Integer> value = new TreeSet<Integer>();
		value.add(2);
		value.add(1);
		System.out.println(value);
		System.out.println(value.add(3));
		System.out.println(value.add(3));
		System.out.println(value);
		System.out.println(value.remove(2));
		System.out.println(value.remove(2));
		System.out.println(value);
	}
}
