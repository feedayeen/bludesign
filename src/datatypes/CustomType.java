package datatypes;

import java.util.Collections;
import java.util.List;

import controlstructures.Function;

public class CustomType	extends Type
{
	private TypeTemplate template;
	private Type[] members;
	
	CustomType(TypeTemplate template, Type[] members)
	{
		this.template = template;
		this.members = members;
	}
	
	//Calls the member or function associated with the object
	public Type call(String name, Type... args)
	{
		Function function = template.getFunction(name);
		return null;
	}
	
	public Type access(String name)
	{
		return template.getMember(name,members);
	}
	
	public String getType()
	{
		return template.typeName;
	}

	public List<String> getMemberNamesNames()
	{
		return Collections.unmodifiableList(template.memberNames);
	}
	
	public List<String> getFunctionNames()
	{
		return Collections.unmodifiableList(template.functionNames);
	}

	@Override
	public boolean toBool() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public double toNumeric() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String toText() {
		// TODO Auto-generated method stub
		return null;
	}
}
