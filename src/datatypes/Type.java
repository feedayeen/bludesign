package datatypes;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import controlstructures.Function;

public abstract class Type
{
	String typename;
	
	public abstract Type call(String name, Type... args);
	
	public abstract Type access(String name);
	
	public abstract String getType();

	public abstract List<String> getMemberNamesNames();
	
	public abstract List<String> getFunctionNames();
	
	public abstract boolean toBool();
	
	public abstract double toNumeric();
	
	public abstract String toText();
}
