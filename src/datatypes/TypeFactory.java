package datatypes;

import java.util.*;

import datatypes.builtin.*;

/**
 * This is a statically accessible factory used to construct all data types
 * First you need to declare which data types are available, built in types excluded
 * Then call the names of each data type with their parameters to build them during the run phase
 * @author feedayeen
 */
public class TypeFactory {
	private static HashMap<String, TypeTemplate> templates = new HashMap<String, TypeTemplate>();
	//The built in types are inserted when constructed
	private static List<String> typeNames = Arrays.asList("Numeric","Text","Bool");
	
	/**
	 * Adds new data type to the factory
	 * @param name of the data type
	 * @param template for the data type
	 */
	public static void addTemplate(String name, TypeTemplate template)
	{
		templates.put(name, template);
		typeNames.add(name);
	}
	
	/**
	 * Lists all of the valid data types in the factory right now
	 * @return the list of data types
	 */
	public static List<String> listTypes()
	{
		System.out.println("listTypes");
		return Collections.unmodifiableList(typeNames);
	}
	
	/**
	 * Constructs a new instance of a given data type with the given parameters
	 * @param name of the data type
	 * @param args arguments for the constructor
	 * @return the data type if all parameters are valid, otherwise it returns null
	 */
	public static Type construct(String name, Type... args)
	{
		TypeTemplate temp = templates.get(name);
		if(temp == null)
		{
			return null;
		}
		return temp.construct(args);
	}
	
	public static Type builtin(double data)
	{
		return new Numeric(data);
	}
	public static Type builtin(boolean data)
	{
		return new Bool(data);
	}
	public static Type builtin(String data)
	{
		return new Text(data);
	}
	public static Type builtin(String type, int size)
	{
		return new Array(type, size);
	}
}
