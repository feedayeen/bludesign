package datatypes;

import java.util.*;

import controlstructures.Function;

public class TypeTemplate
{
	String typeName;
	
	HashMap<String, Integer> namesIndexMap = new HashMap<String, Integer>();
	ArrayList<String> memberNames = new ArrayList<String>();
	ArrayList<String> memberTypes = new ArrayList<String>();
	
	HashMap<String, Function> functionsMap = new HashMap<String, Function>();
	ArrayList<String> functionNames = new ArrayList<String>();
	
	TypeTemplate()
	{
		
	}
	
	public void addMember(String name, Type member)
	{
		namesIndexMap.put(name, memberNames.size());
		memberNames.add(name);
		memberTypes.add(member.getType());
	}
	
	public void addFunction(Function function)
	{
		functionsMap.put(function.getName(),function);
		functionNames.add(function.getName());
	}
	
	/**
	 * Calls the data type's constructor with the arguments
	 * @param args
	 * @return instance of the data type if valid otherwise null
	 */
	Type construct(Type...args)
	{
		Type[] members = new Type[memberNames.size()];
		Type data = new CustomType(this, members);
		return null;
	}
	
	Function getFunction(String name)
	{
		return functionsMap.get(name);
	}
	
	Type getMember(String name, Type[] members)
	{
		try
		{
			int index = namesIndexMap.get(name);
			return members[index];
		}catch(Exception e){return null;} 
	}
}
