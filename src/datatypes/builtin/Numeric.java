package datatypes.builtin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import datatypes.*;

public class Numeric extends Type
{
	double value;
	static String[] functions = {"=","+","-","*","/","%","++","--","==","!=",">","<",">=","<="};
	
	public Numeric(double value)
	{
		this.value = value;
	}
	
	public Type call(String name, Type... args)
	{
		if(args.length == 0)
		{
			switch(name)
			{
			case "++":	//Increment
				value++;
				return this;
			case "--":	//Decrement
				value--;
				return this;
			}
		}
		if(args.length == 1)
		{
			double arg = 0;
			if(args[0] instanceof Numeric)
			{
				Numeric temp = (Numeric)(args[0]);
				arg = temp.value;
			}
			switch(name)
			{
			case "=":	//Assignment
				value = arg;
				return this;
			case "+":	//Addition
				return new Numeric(value+arg);
			case "-":	//Subtraction
				return new Numeric(value-arg);
			case "*":	//Multiplication
				return new Numeric(value*arg);
			case "/":	//Division
				return new Numeric(value/arg);
			case "%":	//Modulus
				return new Numeric(value%arg);
			case "==":	//Equality
				return new Bool(value==arg);
			case "!=":	//In equality
				return new Bool(value!=arg);
			case ">":	//Greater
				return new Bool(value>arg);
			case "<":	//Less
				return new Bool(value<arg);
			case ">=":	//Greater and equal
				return new Bool(value>=arg);
			case "<=":	//Less and equal
				return new Bool(value<=arg);
			}
		}
		return null;
	}
	
	public Type access(String name)
	{
		return null;
	}
	
	public String getType()
	{
		return "Int";
	}
	
	public List<String> getMemberNamesNames()
	{
		return new ArrayList<String>();
	}
	
	public List<String> getFunctionNames()
	{
		return Arrays.asList(functions);
	}
	
	public String toString()
	{
		return Double.toString(value);
	}

	@Override
	public boolean toBool() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public double toNumeric() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String toText() {
		// TODO Auto-generated method stub
		return null;
	}
}
