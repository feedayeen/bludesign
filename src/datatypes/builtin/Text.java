package datatypes.builtin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import datatypes.*;

public class Text extends Type
{
	String value;
	static String[] functions = {"=","+","[]","==","!="};
	
	public Text(String value)
	{
		this.value = value;
	}
	
	public Type call(String name, Type... args)
	{
		if(args.length == 1 && name.equals("[]") && args[0] instanceof Numeric)//IndexOf operation
		{
			Numeric arg = (Numeric)args[0];
			char let = value.charAt((int)(Math.round(arg.value)));
			return new Text(Character.toString(let));
		}
		if(args.length == 1)
		{
			String arg = args[0].toString();
			switch(name)
			{
			case "=":	//Assignment
				value = arg;
				return this;
			case "+":	//Catenation
				return new Text(value+arg);
			case "==":	//Equal
				return new Bool(value.equals(arg));
			case "!=":	//Not Equal
				return new Bool(!value.equals(arg));
			}
		}
		return null;
	}
	
	public Type access(String name)
	{
		return null;
	}
	
	public String getType()
	{
		return "Text";
	}
	
	public List<String> getMemberNamesNames()
	{
		return new ArrayList<String>();
	}
	
	public List<String> getFunctionNames()
	{
		return Arrays.asList(functions);
	}
	
	public String toString()
	{
		return value;
	}

	@Override
	public boolean toBool() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public double toNumeric() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String toText() {
		// TODO Auto-generated method stub
		return null;
	}
}