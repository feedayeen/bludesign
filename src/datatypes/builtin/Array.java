package datatypes.builtin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import datatypes.*;

public class Array extends Type
{
	Type[] value;
	String type;
	static String[] functions = {"[]","="};
	
	public Array(String type, int size)
	{
		value = new Type[size];
		this.type = type;
	}
	
	public Type call(String name, Type... args)
	{
		if(args.length == 1)
		{
			if(name.equals("[]"))
			{
				double arg = 0;
				if(args[0] instanceof Numeric)
				{
					Numeric temp = (Numeric)(args[0]);
					arg = temp.value;
				}
				int index = (int)Math.round(arg);
				return value[index];
			}
		}
		if(args.length == 2 && name.equals("="))
		{
			double arg = 0;
			if(args[0] instanceof Numeric)
			{
				Numeric temp = (Numeric)(args[0]);
				arg = temp.value;
			}
			int index = (int)Math.round(arg);
			value[index] = args[1];
			return value[index];
		}
		return null;
	}
	
	public Type access(String name)
	{
		return null;
	}

	public String getType()
	{
		return "Array" + type;
	}
	
	public List<String> getMemberNamesNames()
	{
		return new ArrayList<String>();
	}
	
	public List<String> getFunctionNames()
	{
		return Arrays.asList(functions);
	}

	@Override
	public boolean toBool() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public double toNumeric() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String toText() {
		// TODO Auto-generated method stub
		return null;
	}
}
