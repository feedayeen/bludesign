package datatypes.builtin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import datatypes.*;

public class Bool extends Type
{
	boolean value;
	static String[] functions = {"=","==","!=","!","&","|","^"};
	
	public Bool(boolean value)
	{
		this.value = value;
	}
	
	public Type call(String name, Type... args)
	{
		if(args.length == 0)
		{
			switch(name)
			{
			case "!":	//Not
				value = !value;
				return this;
			}
		}
		if(args.length == 1)
		{
			boolean arg = false;
			if(args[0] instanceof Bool)
			{
				Bool temp = (Bool)(args[0]);
				arg = temp.value;
			}
			switch(name)
			{
			case "=":	//Assignment
				value = arg;
				return this;
			case "==":	//Equal
				return new Bool(value == arg);
			case "!=":	//Not Equal
				return new Bool(value != arg);
			case "&":	//AND
				return new Bool(value && arg);
			case "|":	//OR
				return new Bool(value || arg);
			case "^":	//XOR
				return new Bool(value ^ arg);
			}
		}
		return null;
	}
	
	public Type access(String name)
	{
		return null;
	}
	
	public String getType()
	{
		return "Bool";
	}
	
	public List<String> getMemberNamesNames()
	{
		return new ArrayList<String>();
	}
	
	public List<String> getFunctionNames()
	{
		return Arrays.asList(functions);
	}
	
	public String toString()
	{
		return Boolean.toString(value);
	}

	@Override
	public boolean toBool() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public double toNumeric() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String toText() {
		// TODO Auto-generated method stub
		return null;
	}
}