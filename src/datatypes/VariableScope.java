package datatypes;

import java.util.HashMap;

/**
 * This class stores the list of variable names and their associated datatypes
 * @author feedayeen
 *
 */
public class VariableScope
{
	HashMap<String,Type> hash = new HashMap<String, Type>();
	
	Type get(String name)
	{
		return hash.get(name);
	}
	
	void assign(Type data, String name)
	{
		hash.put(name, data);
	}
}
