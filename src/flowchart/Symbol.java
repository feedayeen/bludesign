package flowchart;

import java.util.Set;
import java.util.HashSet;

import datatypes.Type;

public class Symbol
{
	Symbol[] inputNodes;
	HashSet<Type> memoizationDeclared = new HashSet<Type>();//Value must start initialized to non-null
	//or else it doesn't search predecessors
	
	private void clearMemoization()
	{
		if(memoizationDeclared != null)
		{
			memoizationDeclared = null;
			for(int i = 0; i < inputNodes.length; i++)
			{
				inputNodes[i].clearMemoization();
			}
		}
	}
	
	private HashSet<Type> validVariables()
	{
		if(memoizationDeclared == null)
		{
			HashSet<Type> valid = new HashSet<Type>(variablesDeclared());
			if(inputNodes.length > 0)
			{
				HashSet<Type> temp = new HashSet<Type>(inputNodes[0].validVariables());
				for(int i = 1; i < inputNodes.length; i++)
				{
					temp.retainAll(inputNodes[i].validVariables());
				}
			}
			memoizationDeclared = valid;
		}
		return memoizationDeclared;
	}
	
	/**
	 * This function processes all instructions executed and identifies which variables are declared
	 * @return
	 */
	private HashSet<Type> variablesDeclared()
	{
		//TODO implement me
		return new HashSet<Type>();
	}
	
	/**
	 * This function checks all variables which exist in all of the previous input paths
	 * This means that if a variable is declared in all logical paths, say an if and the corresponding else
	 * then it is valid. If there exists a path where the variable does not exist, then it is invalid.
	 * @return
	 */
	public HashSet<Type> variableList()
	{
		clearMemoization();	//First pass clears all values memorized
		return validVariables();
	}
}
